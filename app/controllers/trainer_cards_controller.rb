class TrainerCardsController < ApplicationController
  before_action :authenticate_trainer!
  before_action :set_trainer_card, only: [:show, :edit, :update, :destroy]
  before_action :set_trainer_and_pokemon

  # GET /trainer_cards
  # GET /trainer_cards.json
  def index
    @trainer_cards = TrainerCard.all
  end

  # GET /trainer_cards/1
  # GET /trainer_cards/1.json
  def show
  end

  # GET /trainer_cards/new
  def new
    @trainer_card = TrainerCard.new
  end

  # GET /trainer_cards/1/edit
  def edit
    @pokemon = Pokemon.all
  end

  # POST /trainer_cards
  # POST /trainer_cards.json
  def create
    @trainer_card = TrainerCard.new(trainer_card_params)

    respond_to do |format|
      if @trainer_card.save
        format.html { redirect_to @trainer_card, notice: 'Trainer card was successfully created.' }
        format.json { render :show, status: :created, location: @trainer_card }
      else
        format.html { render :new }
        format.json { render json: @trainer_card.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /trainer_cards/1
  # PATCH/PUT /trainer_cards/1.json
  def update
    respond_to do |format|
      if @trainer_card.update(trainer_card_params)
        format.html { redirect_to @trainer_card, notice: 'Trainer card was successfully updated.' }
        format.json { render :show, status: :ok, location: @trainer_card }
      else
        format.html { render :edit }
        format.json { render json: @trainer_card.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /trainer_cards/1
  # DELETE /trainer_cards/1.json
  def destroy
    @trainer_card.destroy
    respond_to do |format|
      format.html { redirect_to trainer_cards_url, notice: 'Trainer card was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def for_trainer 
    @trainer_card = TrainerCard.find_by_trainer_id(params[:id])
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_trainer_card
      @trainer_card = TrainerCard.find(params[:id])
    end

    def set_trainer_and_pokemon
      @pokemons = Pokemon.all
      @current_trainer = current_trainer
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def trainer_card_params
      params.require(:trainer_card).permit(:trainer_id, :pokemon_one, :pokemon_two, :pokemon_three, :pokemon_four, :pokemon_five, :pokemon_six)
    end
end
