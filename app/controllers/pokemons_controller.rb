class PokemonsController < ApplicationController
  before_action :authenticate_trainer!
  before_action :set_pokemon, only: [:show, :edit, :update, :destroy]

  # GET /pokemons
  # GET /pokemons.json
  def index
    if current_trainer.email == "profoak@gmail.com"
      redirect_to collect_pokemons_path()
    elsif current_trainer.trainer_card == nil
      redirect_to new_trainer_card_path() 
    else
      @pokemons = Pokemon.all
    end
  end

  # GET /pokemons/1
  # GET /pokemons/1.json
  def show
  end

  # GET /pokemons/new
  def new
    @pokemon = Pokemon.new
  end

  def collect
  end

  # GET /pokemons/1/edit
  def edit
  end

  # POST /pokemons
  # POST /pokemons.json
  def create
    @pokemon = Pokemon.new(pokemon_params)

    respond_to do |format|
      if @pokemon.save
        format.html { redirect_to @pokemon, notice: 'Pokemon was successfully created.' }
        format.json { render :show, status: :created, location: @pokemon }
      else
        format.html { render :new }
        format.json { render json: @pokemon.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /pokemons/1
  # PATCH/PUT /pokemons/1.json
  def update
    respond_to do |format|
      if @pokemon.update(pokemon_params)
        format.html { redirect_to @pokemon, notice: 'Pokemon was successfully updated.' }
        format.json { render :show, status: :ok, location: @pokemon }
      else
        format.html { render :edit }
        format.json { render json: @pokemon.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /pokemons/1
  # DELETE /pokemons/1.json
  def destroy
    @pokemon.destroy
    respond_to do |format|
      format.html { redirect_to pokemons_url, notice: 'Pokemon was successfully destroyed.' }
      format.json { head :no_content }
    end
  end


  def update_create_pokemon 
    pokemon = format_pokemon_ids params[:pokemon][:collection]

    # Also get move types
    get_move_types

    pokemon.each do |idenifier|
      monster = Poke::Dex.get_pokemon(idenifier)
      mon = Pokemon.where(pid: monster[:pid])

      if mon.empty?
        Pokemon.create(monster)
        Rails.logger.info "Pokemon - #{monster[:name]} - was created"
      else
        mon.update(monster)
        Rails.logger.info "Pokemon - #{monster[:name]} - was updated"
      end
    end

    redirect_to pokemons_path()
  end

  def get_move_types
    (1..18).each do |num| 
      movetype = Poke::Dex.get_move_type(num)
      mt = MoveType.where(tid: movetype[:id])

      if mt.empty?
        m = MoveType.new(movetype)
        m.save
        Rails.logger.info "Move Type - #{movetype[:name]} - was created"
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_pokemon
      @pokemon = Pokemon.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def pokemon_params
      params.require(:pokemon).permit(:pid, :name, :weight, :height, :base_experience, :type, :sprite)
    end

    def format_pokemon_ids collection
      if collection.match(/,/) != nil
        collection.split(',')

      elsif collection.match(/\.\./) != nil
        collection = collection.split("..")
        start = collection[0]
        finish = collection[1]

        Range.new(start, finish)
        
      else 
        collection
      end
    end
end
