json.extract! trainer_card, :id, :trainer_id, :pokemon_one, :pokemon_two, :pokemon_three, :pokemon_four, :pokemon_five, :pokemon_six, :created_at, :updated_at
json.url trainer_card_url(trainer_card, format: :json)
