class TrainerCard < ApplicationRecord
    validate :has_at_least_one_pokemon

    def has_at_least_one_pokemon
        values = [self.pokemon_one, self.pokemon_two, self.pokemon_three, self.pokemon_four, self.pokemon_five, self.pokemon_six]

        if !values.any?
            errors.add(:model_years, "Prof. Oak - You cant use this here. At least one pokemon is required.")
        end
    end
end
