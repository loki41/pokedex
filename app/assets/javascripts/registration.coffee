hide_abutton = () ->
    if $(".speech.eleven").hasClass('active')
        $(".abutton").hide()

next_message = (target) ->
    $(".speech:visible").hide().removeClass('active')
    $(".speech." + target).show().addClass('active')
    hide_abutton();

jQuery ->
    $('.abutton').click ->
        element = $(".speech:visible")

        if $(element).hasClass("seven")
            name = $("#trainer_name").val()
            $(".speech.eight").append( name )

        next_message($(element).attr('data-next'))    