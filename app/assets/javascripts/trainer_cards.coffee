remove_active_slot = () ->
    $(".slot.active").removeClass('active')

assign_pokemon_to_slot = (pokemon) ->
    pid = $(pokemon).attr('data-id')
    slot = $(".slot.active")
    slotnum = $(slot).attr('data-slot')

    $(slot).html('')
    $(pokemon).clone().appendTo(slot)
    $("#trainer_card_pokemon_" + slotnum).val(pid)

jQuery ->
    $('.slot').click ->
        remove_active_slot()

        $(this).addClass('active')


    $('.pokemon.entry').click ->
        assign_pokemon_to_slot(this)