# README

Welcome to the world of Pokemon!

This app allows trainers to register as competitor for the Pokemon League.

- Register you account
- Create your trainer card 
- Assign your Pokemon team


# Setup

Running 'rake db:seed' will create the Professor Oak user. Allowing you to 
add as many Pokemon as you wish.

Once seeded, signin using the following:

- profoak@gmail.com
- Movie2000

You will be greeted with a "collection" page.

Here you can add pokemon one by one using their name or id

Or you can batch add pokemon using the following. 1..151

The more you add the longer it will take unfortunately.


Once complete. Try registering a new user to see the other side of 

The solution. 


Have Fun!



