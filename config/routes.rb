Rails.application.routes.draw do
  
  resources :trainer_cards
  devise_for :trainers

  authenticated :trainer do
    root 'pokemons#index', as: :authenticated_root
  end

  unauthenticated :trainer do
    root "splash#index", as: :unauthenticated_root
  end
  

  resources :pokemons do
    collection do
      get 'collect'
      post 'update_create_pokemon'
    end
  end

  get 'trainer_card/for_trainer/:id' => 'trainer_cards#for_trainer', as: :trainers_card
end
