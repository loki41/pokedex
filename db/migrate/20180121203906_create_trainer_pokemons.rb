class CreateTrainerPokemons < ActiveRecord::Migration[5.0]
  def change
    create_table :trainer_pokemons do |t|
      t.integer :pid
      t.string :name
      t.string :weight
      t.string :height

      t.timestamps
    end
  end
end
