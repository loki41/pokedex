class CreateTrainerCards < ActiveRecord::Migration[5.0]
  def change
    create_table :trainer_cards do |t|
      t.integer :trainer_id
      t.integer :pokemon_one
      t.integer :pokemon_two
      t.integer :pokemon_three
      t.integer :pokemon_four
      t.integer :pokemon_five
      t.integer :pokemon_six

      t.timestamps
    end
  end
end
