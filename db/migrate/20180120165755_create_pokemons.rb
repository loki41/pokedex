class CreatePokemons < ActiveRecord::Migration[5.0]
  def change
    create_table :pokemons do |t|
      t.integer :pid
      t.string  :name
      t.integer :weight
      t.integer :height
      t.integer :base_experience
      t.integer :typeone
      t.integer :typetwo
      t.string  :sprite

      t.timestamps
    end
  end
end
