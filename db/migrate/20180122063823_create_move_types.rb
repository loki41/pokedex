class CreateMoveTypes < ActiveRecord::Migration[5.1]
  def change
    create_table :move_types do |t|
      t.integer :tid
      t.string :name

      t.timestamps
    end
  end
end
