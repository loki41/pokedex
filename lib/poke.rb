module Poke
  class Dex

    def self.make_request endpoint
      return JSON.parse( RestClient.get(POKEAPI_URL + endpoint + "/") )
    end

    def self.get_pokemon identifier
      data    = make_request "pokemon/#{identifier}"
      pokemon = {}

      pokemon[:pid]             = data["id"]
      pokemon[:name]            = data["name"]
      pokemon[:weight]          = data["weight"]
      pokemon[:height]          = data["height"]
      pokemon[:base_experience] = data["base_experience"]
      pokemon[:typeone]         = detect_type data["types"][0]
      pokemon[:typetwo]         = detect_type data["types"][1]
      pokemon[:sprite]          = data["sprites"]["front_default"]

      return pokemon
    end

    def self.detect_type type
      if !type
        return ""
      end

      url = type["type"]["url"]
      typeid = url.match(/(?<=type\/)\d+/)

      return typeid[0].to_i
    end

    def self.get_move_type identifier
      data = make_request "type/#{identifier}"
      type = {}

      type[:tid]  = data["id"]
      type[:name] = data["name"]

      return type
    end
    
  end
end