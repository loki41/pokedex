require 'test_helper'

class TrainerCardsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @trainer_card = trainer_cards(:one)
  end

  test "should get index" do
    get trainer_cards_url
    assert_response :success
  end

  test "should get new" do
    get new_trainer_card_url
    assert_response :success
  end

  test "should create trainer_card" do
    assert_difference('TrainerCard.count') do
      post trainer_cards_url, params: { trainer_card: { pokemon_five: @trainer_card.pokemon_five, pokemon_four: @trainer_card.pokemon_four, pokemon_one: @trainer_card.pokemon_one, pokemon_six: @trainer_card.pokemon_six, pokemon_three: @trainer_card.pokemon_three, pokemon_two: @trainer_card.pokemon_two, trainer_id: @trainer_card.trainer_id } }
    end

    assert_redirected_to trainer_card_url(TrainerCard.last)
  end

  test "should show trainer_card" do
    get trainer_card_url(@trainer_card)
    assert_response :success
  end

  test "should get edit" do
    get edit_trainer_card_url(@trainer_card)
    assert_response :success
  end

  test "should update trainer_card" do
    patch trainer_card_url(@trainer_card), params: { trainer_card: { pokemon_five: @trainer_card.pokemon_five, pokemon_four: @trainer_card.pokemon_four, pokemon_one: @trainer_card.pokemon_one, pokemon_six: @trainer_card.pokemon_six, pokemon_three: @trainer_card.pokemon_three, pokemon_two: @trainer_card.pokemon_two, trainer_id: @trainer_card.trainer_id } }
    assert_redirected_to trainer_card_url(@trainer_card)
  end

  test "should destroy trainer_card" do
    assert_difference('TrainerCard.count', -1) do
      delete trainer_card_url(@trainer_card)
    end

    assert_redirected_to trainer_cards_url
  end
end
